
#tutorial

[docs](https://docs.ignite.com/guide/hello-world)
# create blockchain

```
ignite scaffold chain hello
```

this creates a blockchain called hello

---
# add custom query

```
ignite scaffold query say-hello name --response name
```

add changes to x/hello/keeper/query_say_hello.go

---
# start the blockchain

```
ignite chain serve
```

---
# test the query

```
hellod q hello say-hello world
```

